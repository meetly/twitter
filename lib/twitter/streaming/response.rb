require 'buftok'
require 'http'
require 'json'
require 'twitter/error'

module Twitter
  module Streaming
    class Response
      # Initializes a new Response object
      #
      # @return [Twitter::Streaming::Response]
      def initialize(&block)
        @block     = block
        @parser    = Http::Parser.new(self)
        @tokenizer = BufferedTokenizer.new("\r\n")
      end

      def <<(data)
        begin
          @parser << data
        rescue HTTP::Parser::Error
          puts "HTTP::Parser::Error!!! bypassing small bug in twitter client library.. if this occurs frequently investigate!"
        end 
      end

      def on_headers_complete(_headers)
        error = Twitter::Error::ERRORS[@parser.status_code]
        fail error if error
      end

      def on_body(data)
        begin
          @tokenizer.extract(data).each do |line|
            next if line.empty?
            @block.call(JSON.parse(line, symbolize_names: true, quirks_mode: true))
          end
        rescue JSON::ParserError
          puts "JSON::ParserError!!! bypassing small bug in twitter client library.. if this occurs frequently investigate!"
          puts
        end 
      end
    end
  end
end
